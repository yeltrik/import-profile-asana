<input
    type="checkbox"
    name="import_tags"
    value="import_tags"
>

<label
    for="tags"
>
    Import with Tags :
</label>

<input
    type="text"
    id="tags"
    name="tags"
    value=""
>

<label
    for="tags"
>
    (Ex: "Staff, Retired")
</label>
