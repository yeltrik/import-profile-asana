<input
    type="checkbox"
    name="set_new_user_password"
    value="set_new_user_password"
    disabled
>

<label
    for="user_password"
>
    Set Password for New Users :
</label>

<input
    type="text"
    id="user_password"
    name="user_password"
    value=""
>

<label
    for="user_password"
>
    (Ex: "P@$$w0rD")
</label>
