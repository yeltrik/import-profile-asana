<input
    type="radio"
    name="import_method"
    value="export_json"
    disabled
>

<label
    for="asana_export_json_file"
>
    Import By

    @if(env('ASANA_PROFILE_GID') != "" )
        <a
            href="https://app.asana.com/api/1.0/projects/{{ env('ASANA_PROFILE_GID') }}/tasks?opt_pretty&opt_expand=(this%7Csubtasks%2B)"
            target="_blank"
        >
            Asana Export JSON
        </a>
    @else
        Asana Export JSON
    @endif
    file:
</label>

<input
    type="file"
    id="asana_export_json_file"
    name="asana_export_json_file"
    value="{{ env('ASANA_PROFILE_GID') }}"
>

