<input
    type="checkbox"
    name="email_users"
    value="email_users"
    disabled
>

<label
    for="email_users"
>
    Email Users (Emails will be sent with a random password and a link to reset the password)
</label>
