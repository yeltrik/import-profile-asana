<input
    type="radio"
    name="import_method"
    value="export_csv"
    checked
>

<label
    for="asana_export_csv_file"
>
    Import By

    @if(env('ASANA_PROFILE_GID') != "" )
        <a
            href="https://app.asana.com/-/csv?id={{ env('ASANA_PROFILE_GID') }}"
            target="_blank"
        >
            Asana Export CSV
        </a>
    @else
        Asana Export CSV
    @endif
    file:
</label>

<input
    type="file"
    id="asana_export_csv_file"
    name="asana_export_csv_file"
    value="{{ env('ASANA_PROFILE_GID') }}"
>

