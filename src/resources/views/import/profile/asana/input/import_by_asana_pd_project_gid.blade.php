<input
    type="radio"
    name="import_method"
    value="asana_api_project_gid"
    disabled
>

<label
    for="asana_project_gid"
>
    Import from Asana via "Profile" Project Gid :
</label>

<input
    type="text"
    id="asana_project_gid"
    name="asana_project_gid"
    value="{{ env('ASANA_PROFILE_GID') }}"
>
