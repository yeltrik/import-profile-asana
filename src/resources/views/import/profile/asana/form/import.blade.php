<form
    method="POST"
    action="{{ action([\Yeltrik\ImportProfileAsana\app\http\controllers\ImportProfileAsanaCsvController::class, 'store']) }}"
    enctype="multipart/form-data"
>
    @csrf

    @include('importProfileAsana::import.profile.asana.input.all')

    <br>
    @include('importProfileAsana::import.profile.asana.input.import_button')

</form>
