<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportProfileAsana\app\http\controllers\ImportProfileAsanaCsvController;
use Yeltrik\ImportUniAsana\app\http\controllers\ImportUniAsanaUniversityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('import/profile/asana/create',
    [ImportProfileAsanaCsvController::class ,'create'])
    ->name('imports.profiles.asana.create');

Route::post('import/profile/asana',
    [ImportProfileAsanaCsvController::class ,'store'])
    ->name('imports.profiles.asana.store');
