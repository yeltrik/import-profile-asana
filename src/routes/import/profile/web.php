<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportUniAsana\app\http\controllers\ImportUniAsanaUniversityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'asana/web.php';
