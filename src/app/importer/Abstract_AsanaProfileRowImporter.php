<?php


namespace Yeltrik\ImportProfileAsana\app\importer;


use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaCsvRow;
use Yeltrik\ImportProfileAsana\app\models\ProfileAsanaTask;

Abstract class Abstract_AsanaProfileRowImporter extends Abstract_ImportAsanaCsvRow
{

    /**
     * @param array $row
     * @return HigherOrderBuilderProxy|mixed|null
     */
    protected function getProfileFromRow()
    {
        $gid = $this->row()['Task ID'];
        $profileAsanaTask = ProfileAsanaTask::query()
            ->where('asana_gid', '=', $gid)
            ->first();
        if ($profileAsanaTask instanceof ProfileAsanaTask) {
            return $profileAsanaTask->profile;
        } else {
            return NULL;
        }
    }

}
