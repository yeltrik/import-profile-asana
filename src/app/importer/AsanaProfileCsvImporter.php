<?php

namespace Yeltrik\ImportProfileAsana\app\importer;

use Illuminate\Http\Request;
use Yeltrik\ImportProfileAsana\app\Abstract_AsanaProfileImporter;
use Yeltrik\ImportProfileAsana\app\AsanaProfileCreator;
use Yeltrik\ImportProfileAsana\app\AsanaProfileUpdater;

class AsanaProfileCsvImporter extends Abstract_AsanaProfileImporter
{

    /**
     * @return RedirectResponse
     */
    public function process()
    {
        foreach ($this->csv() as $row) {
            $this->processRow($row);
        }

        return back()
            ->with('success', 'You have successfully upload file.');
    }

    /**
     * @param array $row
     */
    private function processRow(array $row)
    {
        $asanaProject = $this->request()['asana_project'];
        $projectsStr = $row['Projects'];
        $projects = array_map('trim', explode(',', $projectsStr));
        if ( in_array($asanaProject, $projects)) {
            if (
                $this->request()->import_new_profiles &&
                AsanaProfileCreator::rowIsNew($row)
            ) {
                $asanaProfileCreator = new AsanaProfileCreator($this->request(), $row);
                $asanaProfileCreator->process();
                $this->processAttributes($row);
            }

            if (
                $this->request()->update_existing_profiles &&
                AsanaProfileUpdater::rowExists($row)
            ) {
                $asanaProfileUpdater = new AsanaProfileUpdater($this->request(), $row);
                $asanaProfileUpdater->process();
                $this->processAttributes($row);
            }
        }
    }

    private function processAttributes($row)
    {
        $asanaProfileAttributeImporter = new AsanaProfileAttributeImporter($this->request(), $row);
        $asanaProfileAttributeImporter->process();
    }

    /**
     * @param Request $request
     * @return array|void
     */
    public static function validate(Request $request)
    {
        // Validate only if Option was to Upload
        $request->validate([
            'asana_export_csv_file' => 'required|mimes:txt|max:20480',
            'asana_project' => 'required'
        ]);
    }

}
