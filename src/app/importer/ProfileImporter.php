<?php


namespace Yeltrik\ImportProfileAsana\app\importer;


use Illuminate\Database\Eloquent\Builder;
use Yeltrik\Profile\app\models\Profile;

class ProfileImporter
{

    /**
     * @param array $data
     * @return Profile|null
     */
    public static function process(array $data): ?Profile
    {
        $profile = static::profile($data);
        if ($profile instanceof Profile) {
            //CorporateTitleImporter::process($data, $profile);
            EmailImporter::process($data, $profile);
            //NicknameImporter::process($data, $profile);
            PersonalNameImporter::process($data, $profile);
            //ProfileTagImporter::process($data, $profile);
            return $profile;
        } else {
            return NULL;
        }
    }

    /**
     * @param array $data
     * @return Profile|null
     */
    public static function profile(array $data): ?Profile
    {
        $profilesQuery = static::profilesQuery($data);
        if ($profilesQuery === NULL) {
            $profile = NULL;
        } else if ($profilesQuery->count() == 0) {
            $profile = new Profile();
            $profile->save();
        } else if ($profilesQuery->count() == 1) {
            $profile = $profilesQuery->first();
        } else {
            dd([
                'TODO: Merge Profiles',
                $data,
                $profilesQuery->get()
            ]);
            //$profile = MergeProfiles::byQuery($profilesQuery);
        }
        return $profile;
    }

    /**
     * @param array $data
     * @return Builder|null
     */
    public static function profilesQuery(array $data): ?Builder
    {
        $query = Profile::query();

        $emailQuery = EmailImporter::query($data);
        if ($emailQuery instanceof Builder) {
            $query->whereIn('id',
                $emailQuery->pluck('profile_id')
                    ->toArray()
            );
        }

        $personalNameQuery = PersonalNameImporter::query($data);
        if ($personalNameQuery instanceof Builder) {
            $query->whereIn('id',
                $personalNameQuery->pluck('profile_id')
                    ->toArray()
            );
        }

        if (sizeof($query->getQuery()->wheres) > 0) {
            return $query;
        } else {
            return NULL;
        }
    }

}
