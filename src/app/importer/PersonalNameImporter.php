<?php


namespace Yeltrik\ImportProfileAsana\app\importer;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;

class PersonalNameImporter
{

    /**
     * @param array $data
     * @param Profile $profile
     * @return PersonalName|null
     */
    public static function createPersonalName(array $data, Profile $profile): ?PersonalName
    {
        if (
            array_key_exists("First Name", $data)
            && $data['First Name'] != NULL
            && array_key_exists("Last Name", $data)
            && $data['Last Name'] != NULL
        ) {
            $personalName = new PersonalName();
            $personalName->profile()->associate($profile);

            if (
                array_key_exists("First Name", $data)
                && $data['First Name'] != NULL
            ) {
                $personalName->first = $data['First Name'];
            }

            if (
                array_key_exists("Last Name", $data)
                && $data['Last Name'] != NULL
            ) {
                $personalName->last = $data['Last Name'];
            }

            $personalName->save();
            return $personalName;
        } else {
            return NULL;
        }
    }

    /**
     * @param array $data
     * @return Builder|Model|object|null
     */
    protected static function personalNameModel(array $data)
    {
        $query = static::query($data);
        if ($query instanceof Builder) {
            return static::query($data)
                ->first();
        } else {
            return NULL;
        }
    }

    /**
     * @param array $data
     * @param Profile $profile
     * @return PersonalName|null
     */
    public static function process(array $data, Profile $profile): ?PersonalName
    {
        $personalName = static::personalNameModel($data);
        if ($personalName instanceof PersonalName) {
            return $personalName;
        } else {
            return static::createPersonalName($data, $profile);
        }
    }

    /**
     * @param array $data
     * @return Builder|null
     */
    public static function query(array $data): ?Builder
    {
        $query = PersonalName::query();
        $whereUsed = FALSE;
        if (
            array_key_exists("First Name", $data)
            && $data['First Name'] != NULL
        ) {
            $query->where('first', '=', $data['First Name']);
            $whereUsed = TRUE;
        }
        if (
            array_key_exists("Last Name", $data)
            && $data['Last Name'] != NULL
        ) {
            $query->where('last', '=', $data['Last Name']);
            $whereUsed = TRUE;
        }
        if ($whereUsed) {
            return $query;
        } else {
            return NULL;
        }
    }

}
