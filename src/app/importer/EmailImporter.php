<?php


namespace Yeltrik\ImportProfileAsana\app\importer;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\Profile;

class EmailImporter
{

    /**
     * @return Email|null
     */
    public static function createEmail(array $data, Profile $profile)
    {
        $emailStr = static::emailStr($data);
        if ($emailStr == NULL) {
            return NULL;
        } else {
            $email = new Email();
            $email->profile()->associate($profile);
            $email->email = $emailStr;
            $email->save();
            return $email;
        }
    }

    /**
     * @param array $data
     * @return Builder|Model|object|null
     */
    protected static function emailModel(array $data)
    {
        if (static::emailStr($data) == NULL) {
            return NULL;
        } else {
            return static::query($data)
                ->first();
        }
    }

    /**
     * @param array $data
     * @return string|null
     */
    protected static function emailStr(array $data)
    {
        if (array_key_exists('Email', $data)) {
            return trim(
                strtolower(
                    $data['Email']
                )
            );
        } else {
            return NULL;
        }
    }

    /**
     * @param array $data
     * @param Profile $profile
     * @return Email|null
     */
    public static function process(array $data, Profile $profile): ?Email
    {
        $email = static::emailModel($data);
        if ($email instanceof Email) {
            return $email;
        } else {
            return static::createEmail($data, $profile);
        }
    }

    /**
     * @param array $data
     * @return Builder|null
     */
    public static function query(array $data): ?Builder
    {
        $emailStr = static::emailStr($data);
        if ( $emailStr != NULL ) {
            return Email::query()
                ->where('email', '=', $emailStr);
        } else {
            return NULL;
        }
    }

}
