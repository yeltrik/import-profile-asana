<?php


namespace Yeltrik\ImportProfileAsana\app\importer;


use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\ProfileTag;

class AsanaProfileAttributeImporter extends Abstract_AsanaProfileRowImporter
{

    public function process()
    {
        $this->processEmail();
        $personalName = $this->processPersonalName();
        $this->processTags();
        if ( $personalName instanceof PersonalName ) {
            $this->processUser($personalName);
        }
    }

    private function processEmail()
    {
        $profile = $this->getProfileFromRow();
        $emailStr = $this->row()['Email'];
        if ($emailStr != NULL && isset($this->request()['import_email'])) {
            if ( !Email::query()->where('email', '=', $emailStr)->exists() ) {
                $email = new Email();
                $email->profile()->associate($profile);
                $email->email = $emailStr;
                $email->save();
            }
        }
    }

    /**
     * @return PersonalName|null
     */
    private function processPersonalName()
    {
        $profile = $this->getProfileFromRow();
        $first = $this->row()['First Name'];
        $last = $this->row()['Last Name'];
        if ($first != NULL && $last != NULL) {
            $personalName = PersonalName::query()->where('profile_id', '=', $profile->id)->first();
            if ($personalName instanceof PersonalName == FALSE) {
                $personalName = new PersonalName();
                $personalName->profile()->associate($profile);
            }
            $personalName->first = $first;
            $personalName->last = $last;
            $personalName->save();
            return $personalName;
        }
        return NULL;
    }

    /**
     *
     */
    private function processTags()
    {
        $profile = $this->getProfileFromRow();
        if (
            isset($this->request()['import_tags'])
            && isset($this->request()['tags'])
            && $this->request()['tags'] != NULL
        ) {
            $tags = array_map('trim', explode(',', $this->request()['tags']));
            foreach ($tags as $tag) {
                $profileTag = ProfileTag::query()
                    ->where('profile_id', '=', $profile->id)
                    ->where('tag', '=', $tag)
                    ->first();
                if ($profileTag instanceof ProfileTag == FALSE) {
                    $profileTag = new ProfileTag();
                    $profileTag->profile()->associate($profile);
                    $profileTag->tag = $tag;
                    $profileTag->save();
                }
            }
        }
    }

    /**
     * @param PersonalName $personalName
     */
    private function processUser(PersonalName $personalName)
    {
        $emailStr = $this->row()['Email'];
        if ($emailStr != NULL && isset($this->request()['create_users'])) {
            // TODO: Check for Existing User first
            if (!User::query()->where('email', '=', $emailStr)->exists()) {
                $user = new User();
                $user->name = $personalName->fullName;
                $user->email = $emailStr;
                $user->password = Hash::make(Str::random(36));
                $user->save();
            }
        }
    }

}
