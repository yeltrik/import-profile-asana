<?php


namespace Yeltrik\ImportProfileAsana\app;


use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yeltrik\ImportProfileAsana\app\importer\Abstract_AsanaProfileRowImporter;
use Yeltrik\ImportProfileAsana\app\models\ProfileAsanaTask;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\Profile\app\models\ProfileTag;

class AsanaProfileUpdater extends Abstract_AsanaProfileRowImporter
{

    /**
     *
     */
    public function process()
    {
        if (static::rowExists($this->row())) {
            $profile = $this->getProfileFromRow($this->row());
            if ($profile instanceof Profile) {
                //
            } else {
                dd([
                    'Task does not exist',
                    $this->row()
                ]);
            }
        }
    }

    /**
     * @param array $task
     * @return bool
     */
    public static function rowExists(array $task)
    {
        $gid = $task['Task ID'];
        $profileAsanaTask = ProfileAsanaTask::query()
            ->where('asana_gid', '=', $gid)
            ->first();

        return ($profileAsanaTask instanceof ProfileAsanaTask);
    }

}
