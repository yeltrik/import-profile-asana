<?php


namespace Yeltrik\ImportProfileAsana\app;


use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yeltrik\ImportProfileAsana\app\importer\Abstract_AsanaProfileRowImporter;
use Yeltrik\ImportProfileAsana\app\models\ProfileAsanaTask;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\Profile\app\models\ProfileTag;

class AsanaProfileCreator extends Abstract_AsanaProfileRowImporter
{
    /**
     *
     */
    public function process()
    {
        $asanaProject = $this->request()['asana_project'];

        if (static::rowIsNew($this->row())) {
            $gid = $this->row()['Task ID'];
            $first = $this->row()['First Name'];
            $last = $this->row()['Last Name'];

            $profile = new Profile();
            $profile->save();



            $profileAsanaTask = new ProfileAsanaTask();
            $profileAsanaTask->profile()->associate($profile);
            $profileAsanaTask->asana_gid = $gid;
            $profileAsanaTask->save();

            $emailStr = $this->row()['Email'];
            if ($emailStr != NULL && isset($this->request()['import_email'])) {
                $email = new Email();
                $email->profile()->associate($profile);
                $email->email = $emailStr;
                $email->save();
            }
        }
    }

    public static function rowIsNew(array $row)
    {
        return !AsanaProfileUpdater::rowExists($row);
    }

}
