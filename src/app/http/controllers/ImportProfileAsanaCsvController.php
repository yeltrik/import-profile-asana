<?php

namespace Yeltrik\ImportProfileAsana\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\ImportProfileAsana\app\importer\AsanaProfileCsvImporter;

/**
 * Class ImportProfileAsanaCsvImporter
 * @package Yeltrik\ImportProfileAsana\app\http\controllers
 */
class ImportProfileAsanaCsvController extends Controller
{

    /**
     * ImportProfileAsanaCsvImporter constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        // TODO: Authorize
        return view("importProfileAsana::import.profile.asana.create");
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        switch($request->import_method) {
            case "export_csv":
                AsanaProfileCsvImporter::validate($request);
                $importer = new AsanaProfileCsvImporter($request);
                return $importer->process();
            default:
                dd('Unsupported Import Method: ' . $request->import_method);
        }
    }

}
