<?php

namespace Yeltrik\ImportProfileAsana\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\Profile\app\models\Profile;

/**
 * Class ProfileAsanaTask
 *
 * @property int profile_id
 * @property string asana_gid
 *
 * @property Profile profile
 *
 * @package Yeltrik\ImportProfileAsana\app\models
 */
class ProfileAsanaTask extends Model
{
    use HasFactory;

    protected $connection = 'import_profile_asana';
    public $table = 'profile_asana_task';

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

}
